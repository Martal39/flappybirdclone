# Learning to make Flappy Bird

![](/assets/screenshot.png "My Flappy Bird Clone")

Made as part of the https://20_games_challenge.gitlab.io/challenge/

Using the font Upheaval, created by Brian Kent. Other assets made by myself.

