extends KinematicBody2D

class_name Bird

const GRAVITY = 98.0 * 5;
const UPWARD_ACCELERATION = GRAVITY
const TERMINAL_VELOCITY_DOWN = 600.0
const TERMINAL_VELOCITY_UP = -400.0
var velocity = Vector2.ZERO
var game_over = false


func _ready():
	$AnimationPlayer.play("RESET")
	$AnimationPlayer.connect("animation_finished", self, "on_anim_end")
	Events.create_game_over_signal(self)
	pass # Replace with function body.

func _physics_process(delta):
	if game_over:
		return
	velocity.y += GRAVITY * delta
	velocity.x = Constants.SPEEDS.PIPE
	
	if Input.is_action_just_pressed("ui_accept"):
		velocity.y = UPWARD_ACCELERATION * -1
		$AnimationPlayer.play("flap")
	# Cap velocity
	
	velocity.y = clamp(velocity.y, TERMINAL_VELOCITY_UP, TERMINAL_VELOCITY_DOWN)
	var motion = velocity * delta
	move_and_collide(motion)
	
	
#	if Input.is_action_just_pressed("ui_accept"):
#		velocity.y += UPWARD_ACCELERATION * delta
	
	
func on_anim_end(anim):
	print("ANIM is ", anim)
	if anim == 'flap':
		$AnimationPlayer.play("RESET")
	
func on_hit():
	game_over = true
	$AnimationPlayer.play("game_over")
	
func on_game_over():
	game_over = true
	var tween = get_node("Tween")
	tween.interpolate_property($CollisionPolygon2D/Bird, "global_position",
		global_position, Vector2(global_position.x + 1920, global_position.y), 1,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
