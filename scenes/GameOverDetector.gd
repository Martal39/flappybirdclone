extends Area2D


func _on_GameOverDetector_body_entered(body):
	if body.is_in_group("Bird"):
		Events.emit_signal("game_over")
