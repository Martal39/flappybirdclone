extends Sprite

export (float) var speed = 100.0;

func _physics_process(delta):
	position.x -= speed * delta
