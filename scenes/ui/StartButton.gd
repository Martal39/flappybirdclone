extends Button

export (String, FILE) var level

func _on_StartButton_pressed():
	assert(level, "Level not set in StartButton")
	LevelManager.goto_scene(level)

