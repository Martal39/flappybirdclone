extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	Events.connect("game_over", self, "on_game_over")

func on_game_over():
	$AnimationPlayer.play("FADE_IN")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
