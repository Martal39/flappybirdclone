extends HBoxContainer

var score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$ScoreValue.text = str(score)
	Events.connect_tick_up_score(self)
	pass # Replace with function body.


func on_tick_up_score():
	score += 1000
	$ScoreValue.text = str(score)
