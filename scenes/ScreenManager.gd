extends Node2D

export(String, FILE) var scene_file
var preloaded_scene

var screens_passed = 0

func _ready():
	Events.connect("spawn_new_screen", self, "spawn_screen")
	preloaded_scene = load(scene_file)
	pass # Replace with function body.


func spawn_screen():
	var screens = get_children()
	var last_screen = screens.back()
	
	var new_screen = preloaded_scene.instance()
	add_child(new_screen)
	new_screen.position.x = last_screen.position.x + 1920
	
	if screens.size() > 3:
		screens[0].queue_free()
	
