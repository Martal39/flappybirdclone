extends Node2D
class_name CollidableObject

# Called when the node enters the scene tree for the first time.
func _ready():
	var area2d: Area2D = find_node("Area2D")
	if area2d:
		area2d.connect("body_entered", self, "on_body_entered")

func on_body_entered(b):
	if b.is_in_group("Bird"):
		var bird: Bird = b
		bird.on_hit()

