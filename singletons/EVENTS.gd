extends Node

signal spawn_new_screen
signal game_over
signal tick_up_score

func connect_tick_up_score(obj, fn = "on_tick_up_score"):
	connect("tick_up_score", obj, fn)

func create_game_over_signal(obj, fn = "on_game_over"):
	connect("game_over", obj, fn)
	
